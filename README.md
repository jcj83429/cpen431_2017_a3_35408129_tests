### How to use ###
java -jar A3Tests.jar -a localhost -p <port> -t <testno>

testno specifies which test. The tests are:

	1: basic test
	   go through each operation once
	2: put stress test
	   fill up the server by spamming put and check the server does not die
	3: transport overload test
	   try to overload the transport layer reply caching with spam requests
	   terminates after getting 10 "system overload" (0x03) replies
	4: reply caching test
	   put a value, get it, remove it, then resend the old get request

If no exception is thrown, it means the test passed.
Tests 2-4 are useful. Nolan Ramsden has a much better test suite for basic operations.